export const state = () => ({
  selectedJob: null,
})

export const mutations = {
  setSelectedJob: (state, job) => {
    state.selectedJob = job
  },
}

export const actions = {
  setSelectedJob({ commit }, job) {
    commit('setSelectedJob', job)
  },
}

# devjobs

This project is implemented with Nuxt framework.

## features

```
 - It's a universal app.
 - It's a PWA app.
 - Dark and light theme.
 - Filters are stored in the URL query string.
 - Skeleton loader is used.
 - Styles are scoped.
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

Here is the deployed projet link: [Devjobs] (https://alibaba-devjobs.herokuapp.com/)

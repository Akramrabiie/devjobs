import axios from './axios'
import css from './css'
import vuetify from './vuetify'

export default {
  axios,
  vuetify,
  css,
}
